module gitee.com/ifinder/aliyun-oss-go-sdk

go 1.13

require (
	github.com/aliyun/alibaba-cloud-sdk-go v1.61.266
	github.com/baiyubin/aliyun-sts-go-sdk v0.0.0-20180326062324-cfa1a18b161f
	github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e // indirect
	github.com/satori/go.uuid v1.2.0 // indirect
	golang.org/x/time v0.0.0-20200416051211-89c76fbcd5d1
	gopkg.in/check.v1 v1.0.0-20200227125254-8fa46927fb4f
)
